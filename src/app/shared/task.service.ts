import {Injectable} from "@angular/core"
import {HttpClient} from "@angular/common/http"
import {map} from "rxjs/operators"
import {Observable} from "rxjs"
import * as moment from "moment"

export interface TaskInterface {
  title: string
  description: string
  id?: string
  date?: string
}

interface CreateResponseInterface {
  name: string
}

@Injectable({ providedIn: 'root' })
export class TaskService {
  static url = 'https://angular-calendar-b63e7.firebaseio.com/tasks'

  constructor(private http: HttpClient) {}

  load(date: moment.Moment): Observable<TaskInterface[]> {
    return this.http
      .get<TaskInterface[]>(`${TaskService.url}/${date.format('DD-MM-YYYY')}.json`)
      .pipe(map(tasks => {
        if(!tasks) {
          return []
        }
        return Object.keys(tasks).map(key => ({...tasks[key], id: key}))
      }))
  }

  create(task: TaskInterface): Observable<TaskInterface> {
    return this.http
      .post<CreateResponseInterface>(`${TaskService.url}/${task.date}.json`, task)
      .pipe(map(({ name }) => ({...task, id: name})))
  }
}
